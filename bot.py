#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Author: Mostafa Ahangarha
# Licence: GPLv3
# Description: This is a telegram bot which helps to make jitsi room eaisly

import logging
import random
import string
import re
from uuid import uuid4

from telegram import InlineQueryResultArticle, ParseMode, \
    InputTextMessageContent
from telegram.ext import Updater, InlineQueryHandler, CommandHandler
from telegram.utils.helpers import escape_markdown
from config import *


def start(update, context):
    text = """Welcome to meet jitsi Bot.

    Here you can easily make a jitsi room for your online video call!

    Choose how to make the room name:"""

    update.message.reply_text(text)
    logging.debug("Responded to /start...")


def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


def make_response(query, mode):
    url = ""
    if mode == "random":
        url = "{}{}".format(BASE_URL, randomword(8))
    else:
        if len(query) == 0:
            url = make_response(query, "random")
        else:
            # clean bad characters
            clean_query = re.sub(r"[^a-zA-Z0-9-]", "", query)
            clean_query = clean_query.replace(" ", "")
            if len(clean_query) == 0:
                make_response('', "random")

            url = "{}{}".format(
                BASE_URL,
                escape_markdown(clean_query)
            )

    return MSG_PREFIX + url


def inlinequery(update, context):
    """Handle the inline query."""

    query = update.inline_query.query
    results = [
        InlineQueryResultArticle(
            id=uuid4(),
            title="Random room name",
            input_message_content=InputTextMessageContent(
                make_response(query, "random"))),
    ]

    if len(query) > 0:
        results.append(InlineQueryResultArticle(
            id=uuid4(),
            title="Custom room name",
            input_message_content=InputTextMessageContent(
                make_response(query, "custom")
            ), parse_mode=ParseMode.MARKDOWN),
        )

    update.inline_query.answer(results)


def main():
    logging.basicConfig(
        format='%(asctime)s - %(levelname)s - %(message)s',
        level=LOG_LEVEL,
    )

    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(InlineQueryHandler(inlinequery))

    updater.start_polling()
    logging.info("The bot is running...")
    updater.idle()


if __name__ == '__main__':
    main()
