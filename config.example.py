# This is sample config file. To use this, make a copy of it and rename it by
# removing '.example' from the filename, then set the variables.

TOKEN = "YOUR BOT TOKEN"

# Log level: INFO|DEBUG
LOG_LEVEL = "INFO"

# Base url of jitsi instance for example: "https://meet.jit.si/"
BASE_URL = "https://meet.jit.si/"

# The message which appears before the jitsi room address
MSG_PREFIX = "I have made a Jitsi room for a video call. \
Please join me using this link: \n\n"
